package com.progressoft.jip12.matrix;

import com.sun.source.tree.BreakTree;

import java.util.Arrays;

public class Matrix {
    private final int[][] matrix;
    private  int rows;
    private  int cols;

    public Matrix(int[][] matrix) {
        rows = matrix.length;
        for (int row = 0; row < rows; row++) {
            if (matrix[row] == null)
                throw new IllegalArgumentException("row at index: " + row + " is null");
        }
        try{
        cols = matrix[0].length;
        }catch (ArrayIndexOutOfBoundsException e){
            cols = 0;
        }


        for (int[] row : matrix) {
            if (row.length != cols)
                throw new IllegalArgumentException("inconsistent columns in matrix");
        }

        this.matrix = new int[rows][cols];
        int c = 0;
        for (int[] row :
                matrix) {
            this.matrix[c++] = Arrays.copyOf(row, row.length);
        }





        /*
        this.matrix = new int[rows][cols];

        for (int i = 0; i < rows; i++) {

        }
    }*/
        //this.matrix = new int[5][5];
    }


    public int getNoOfRows() {
        return rows;
    }

    public int getNoOfCols() {
        return cols;
    }

    public int getElement(int row, int col) {
        return matrix[row][col];
    }


    public boolean equals(Object object) {
        if (!(object instanceof Matrix))
            return false;
        Matrix other = (Matrix) object;
        if (!isSameSize(other)) {
            return false;
        }
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < cols; col++) {
                if (!isSameElement(other, row, col))
                    return false;
            }
        }
        return true;
    }

    private boolean isSameElement(Matrix other, int row, int col) {
        return getElement(row, col) == other.getElement(row, col);
    }

    private boolean isSameSize(Matrix other) {
        return this.rows == other.getNoOfRows() && this.cols == other.getNoOfCols();
    }


}
