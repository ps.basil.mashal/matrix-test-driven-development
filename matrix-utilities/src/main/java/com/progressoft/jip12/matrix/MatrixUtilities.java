package com.progressoft.jip12.matrix;

import java.util.function.BiPredicate;

public class MatrixUtilities {
    public static Matrix sum(Matrix m1, Matrix m2) {
        if (!isSameSize(m1, m2))
            throw new IllegalArgumentException("can,t do sum the given mercies are not the same size!");

        int[][] result = getResultSum(m1, m2);
        return new Matrix(result);
    }

    private static int[][] getResultSum(Matrix m1, Matrix m2) {
        int noOfRows = m1.getNoOfRows();
        int noOfCols = m1.getNoOfCols();
        int[][] result = new int[noOfRows][noOfCols];
        for (int row = 0; row < noOfRows; row++) {
            for (int col = 0; col < noOfCols; col++) {
                result[row][col] = m1.getElement(row, col) + m2.getElement(row, col);
            }
        }
        return result;
    }

    private static boolean isSameSize(Matrix m1, Matrix m2) {
        return (m1.getNoOfRows() == m2.getNoOfRows()) && (m1.getNoOfCols() == m2.getNoOfCols());
    }

    public static Matrix scalar(int scalar, int[][] arr) {
        Matrix matrix = new Matrix(arr);
        int[][] result = new int[arr.length][arr[0].length];
        for (int row = 0; row < matrix.getNoOfRows(); row++) {
            for (int col = 0; col < matrix.getNoOfCols(); col++) {
                result[row][col] = matrix.getElement(row, col) * scalar;
            }
        }

        return new Matrix(result);
    }

    public static Matrix transpose(int[][] arr) {
        Matrix matrix = new Matrix(arr);

        int[][] result = new int[arr[0].length][arr.length];

        for (int row = 0; row < matrix.getNoOfRows(); row++) {
            for (int col = 0; col < matrix.getNoOfCols(); col++) {
                result[col][row] = matrix.getElement(row, col);
            }
        }
        return new Matrix(result);
    }

    public static Matrix matrixMultiplication(int[][] first, int[][] second) {
        isValidMatrix(first, "first");
        isValidMatrix(second, "second");
        if (!isValidForMultiplication(first, second))
            throw new IllegalArgumentException("the matrices are not valid for multiplication, the no of cols in first matrix must match the no of rows in the second matrix");
        int[][] result = new int[first.length][second[0].length];
        for (int row = 0; row < first.length; row++) {
            multiplyRowByColumns(first, second, result, row);
        }
        return new Matrix(result);
    }

    private static void multiplyRowByColumns(int[][] first, int[][] second, int[][] result, int row) {
        for (int colSecond = 0; colSecond < second[0].length; colSecond++) {
            int accum = 0;
            for (int col = 0; col < first[0].length; col++) {
                accum += (first[row][col] * second[col][colSecond]);
            }
            result[row][colSecond] = accum;
        }
    }

    private static boolean isValidForMultiplication(int[][] first, int[][] second) {
        return first[0].length == second.length;

    }

    private static void isValidMatrix(int[][] matrix, String order) {
        try {
            Matrix firstMatrix = new Matrix(matrix);
        } catch (IllegalArgumentException exception) {
            if (isInconsistentColumnsInMatrix(exception)) {
                throw new IllegalArgumentException("the given " + order + " matrix has inconsistent columns");
            } else
                throw new IllegalArgumentException(order + " Matrix has a null " + exception.getMessage());
        }
    }

    private static boolean isInconsistentColumnsInMatrix(IllegalArgumentException exception) {
        return exception.getMessage().equals("inconsistent columns in matrix");
    }

    public static Matrix subMatrix(int[][] matrix, int rowToDelete, int colToDelete) {
        Matrix matrix1 = new Matrix(matrix);
        if (isGivenRowAndColumnOutOfBound(matrix, rowToDelete, colToDelete))
            throw new IllegalArgumentException("the given row and column are out of bounds");
        if (isGivenRowOutOfBound(matrix, rowToDelete))
            throw new IllegalArgumentException("the given row is out of bounds");
        if (isGivenColumnOutOfBound(matrix, colToDelete))
            throw new IllegalArgumentException("the given col is out of bounds");

        int[][] result = new int[matrix.length - 1][matrix[0].length - 1];
        int rowPointer = 0;
        int colPointer = 0;
        for (int row = 0; row < matrix.length; row++) {
            if (row == rowToDelete)
                continue;
            for (int col = 0; col < matrix[0].length; col++) {
                if (col == colToDelete)
                    continue;
                result[rowPointer][colPointer++] = matrix[row][col];
            }
            rowPointer++;
            colPointer = 0;
        }


        return new Matrix(result);
    }

    private static boolean isGivenColumnOutOfBound(int[][] matrix, int colToDelete) {
        return colToDelete >= matrix[0].length;
    }

    private static boolean isGivenRowOutOfBound(int[][] matrix, int rowToDelete) {
        return rowToDelete >= matrix.length;
    }

    private static boolean isGivenRowAndColumnOutOfBound(int[][] matrix, int rowToDelete, int colToDelete) {
        return isGivenColumnOutOfBound(matrix, colToDelete) && isGivenRowOutOfBound(matrix, rowToDelete);
    }

    public static Matrix squareMatrix(int[][] input, Triangular triangular) {
        Matrix matrix = new Matrix(input);
        if (!isSquare(input))
            throw new IllegalArgumentException("the given matrix is not square");
        BiPredicate<Integer, Integer> reset;
        if (triangular == null)
            throw new IllegalArgumentException("the given triangular is null");
        else if (triangular == Triangular.DIAGONAL)
            reset = (i, j) -> i == j;
        else if (triangular == Triangular.UPPER)
            reset = (i, j) -> i <= j;
        else
            reset = (i, j) -> i >= j;

        int[][] result = new int[input.length][input[0].length];
        for (int i = 0; i < input.length; i++) {
            for (int j = 0; j < input.length; j++) {
                if (reset.test(i, j)) {
                    result[i][j] = input[i][j];
                }
            }
        }
        return new Matrix(result);
    }

    private static boolean isSquare(int[][] input) {
        return input.length == input[0].length;
    }
}
