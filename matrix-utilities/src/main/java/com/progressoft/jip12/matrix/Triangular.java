package com.progressoft.jip12.matrix;

public enum Triangular {
    UPPER,
    LOWER,
    DIAGONAL;
}
