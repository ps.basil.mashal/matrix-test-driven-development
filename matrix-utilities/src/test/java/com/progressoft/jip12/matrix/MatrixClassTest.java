package com.progressoft.jip12.matrix;

import com.progressoft.jip12.matrix.Matrix;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.InputMismatchException;
import java.util.regex.Pattern;

public class MatrixClassTest {


    @Test
    public void givenTwoMatricesWithSameValuesAndDifferentAddresses_whenTestingTheEquality_thenComparisonOfEqualityShouldReturnedDependsOnTheValuesRatherThanTheAddresses(){
        //Input
        int[][] a = new int[][]{
                {1,2,3},
                {2,1,4},
                {5,5,5}
        };
        Matrix first = new Matrix(a);
        Matrix second = new Matrix(a);

        boolean expected = true;
        Assertions.assertEquals(first,second);
    }

    @Test
    public void givenMatrixWithNullRow_whenInitializingMatrix_thenThrowException() {
        //input
        int[][] array = new int[][]{
                null,
                {1, 2, 3},
                null
        };

        //expected
        Exception e = Assertions.assertThrows(IllegalArgumentException.class, () -> new Matrix(array));

        /*Pattern pattern = Pattern.compile("row at index: [0-9]+ is null");
        boolean outputMatch = pattern.matcher(e.getMessage()).matches();
        Assertions.assertTrue(outputMatch,"null at row expected while" + e.getMessage() + "is the actual");*/
        //Assertions.assertEquals("row at index: " +  + " is null");

        //output
        Assertions.assertEquals("row at index: 0 is null" , e.getMessage());

    }

    @Test
    public void givenInconsistentColumnsInMatrix_whenInstantiatingMatrix_thenThrowException(){
        //input
        int[][] input = new int[][]{
                {1,2},
                {1,2,3}
        };

        Exception e =Assertions.assertThrows(IllegalArgumentException.class,()->new Matrix(input));

        Assertions.assertEquals("inconsistent columns in matrix" , e.getMessage());
    }

    @Test
    public void givenValidMatrix_whenInstantiatingMatrix_thenAssignToTheMatrix(){
        int[][] input = new int[][]{
                {1,2,3},
                {5,8,11},
                {2,4,6},
                {1,4,5}
        };


        Matrix matrix = new Matrix(input);

        Assertions.assertEquals(4,matrix.getNoOfRows());
        Assertions.assertEquals(3,matrix.getNoOfCols());

        Assertions.assertEquals(5,matrix.getElement(1,0));
        Assertions.assertEquals(11,matrix.getElement(1,2));
        Assertions.assertEquals(2,matrix.getElement(0,1));
        Assertions.assertEquals(3,matrix.getElement(0,2));
        Assertions.assertEquals(6,matrix.getElement(2,2));
        Assertions.assertEquals(5,matrix.getElement(3,2));
    }
}
