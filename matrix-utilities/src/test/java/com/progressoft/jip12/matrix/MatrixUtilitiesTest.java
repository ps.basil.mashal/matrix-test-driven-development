package com.progressoft.jip12.matrix;

import com.progressoft.jip12.matrix.Matrix;
import com.progressoft.jip12.matrix.MatrixUtilities;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.LinkPermission;

public class MatrixUtilitiesTest {

    @Test
    public void givenTwoMatricesWithDifferentSizes_whenSum_thenThrowIllegalArgumentException() {
        //in
        Matrix m1 = new Matrix(new int[][]{
                {1, 2, 3},
                {2, 1, 4}
        });
        Matrix m2 = new Matrix(new int[][]{
                {1, 2},
                {2, 1}
        });

        Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.sum(m1, m2));
    }

    @Test
    public void givenTwoValidMatrices_whenSum_thenTheResultIsReturned() {
        Matrix m1 = new Matrix(new int[][]{
                {1, 2, 3},
                {2, 1, 4}
        });
        Matrix m2 = new Matrix(new int[][]{
                {1, 2, 7},
                {2, 1, 0}
        });

        Matrix actual = MatrixUtilities.sum(m1, m2);
        Matrix expected = new Matrix(new int[][]{
                {2, 4, 10},
                {4, 2, 4}
        });

        Assertions.assertEquals(expected, actual, "the sum result is not as expected");

    }

    @Test
    public void givenScalarAndValidMatrix_whenScalarMultiplication_thenResultIsReturned() {
        //Input
        int scalar = 2;
        int[][] arr = new int[][]{
                {1, 21, 3},
                {5, 1, 2}
        };

        //expected

        Matrix expected = new Matrix(new int[][]{
                {2, 42, 6},
                {10, 2, 4}
        });

        //action

        Matrix actual = MatrixUtilities.scalar(2, arr);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void givenValidMatrix_whenTranspose_thenResultIsReturned() {
        int[][] arr = new int[][]{
                {1, 2, 4, 6},
                {5, 4, 7, 11}
        };

        Matrix expected = new Matrix(new int[][]{
                {1, 5},
                {2, 4},
                {4, 7},
                {6, 11}
        });

        Matrix actual = MatrixUtilities.transpose(arr);

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void givenFirstMatrixHasNullRow_whenMatrixMultiplication_thenIllegalArgumentExceptionIsThrown() {
        int[][] first = new int[][]{
                null,
                {2, 4},
                {1, 5}
        };

        int[][] second = new int[][]{
                {1, 4, 2, 3},
                {1, 4, 5, 8}
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.matrixMultiplication(first, second));
        Assertions.assertEquals("first Matrix has a null row at index: 0 is null", exception.getMessage());
    }

    @Test
    public void givenSecondMatrixHasNullRow_whenMatrixMultiplication_thenIllegalArgumentExceptionIsThrown() {
        int[][] first = new int[][]{
                {1, 1},
                {2, 4},
                {1, 5}
        };

        int[][] second = new int[][]{
                {1, 4, 2, 3},
                null
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.matrixMultiplication(first, second));
        Assertions.assertEquals("second Matrix has a null row at index: 1 is null", exception.getMessage());
    }

    @Test
    public void givenFirstMatrixWithInconsistentColumns_whenMatrixMultiplication_thenIllegalArgumentExceptionIsThrown() {
        int[][] first = new int[][]{
                {1, 2, 4, 7},
                {1, 2, 4}
        };
        int[][] second = new int[][]{
                {1, 2},
                {5, 3},
                {7, 8},
                {5, 5}
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.matrixMultiplication(first, second));
        Assertions.assertEquals("the given first matrix has inconsistent columns", exception.getMessage());
    }

    @Test
    public void givenMatricesAreNotMultiplicable_whenMatrixMultiplication_thenExceptionIsThrown() {
        int[][] first = new int[][]{
                {5, 7, 3},
                {7, 2, 1}
        };
        int[][] second = new int[][]{
                {5, 7, 3},
                {7, 2, 1}
        };
        //the given two matrices are not valid for multiply because the noOfCols
        //for the first matrix has to match the noOfRows of the second Matrix

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.matrixMultiplication(first, second));
        Assertions.assertEquals("the matrices are not valid for multiplication, the no of cols in first matrix must match the no of rows in the second matrix", exception.getMessage());
    }

    @Test
    public void givenTwoValidMatricesAndSatisfyTheMultiplicationConditions_whenMatrixMultiplication_thenResultIsReturned() {
        int[][] first = new int[][]{
                {1, 2, 4, 7},
                {1, 2, 4, 2}
        };
        int[][] second = new int[][]{
                {1, 2},
                {5, 3},
                {7, 8},
                {5, 5}
        };


        Matrix expected = new Matrix(new int[][]{
                {74, 75},
                {49, 50}
        });

        Matrix actual = MatrixUtilities.matrixMultiplication(first, second);
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void givenInvalidMatrixContainsNullRowOrInconsistentCols_whenSubMatrix_thenThrowException() {
        int[][] a = new int[][]{
                null,
                {1, 2, 4},
                {5, 1, 4}
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.subMatrix(a, -1, -1));
        Assertions.assertEquals("row at index: 0 is null", exception.getMessage());

        int[][] b = new int[][]{
                {1, 2, 4, 5},
                {5, 2, 4},
                {5, 2, 11}
        };
        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.subMatrix(b, -1, -1));
        Assertions.assertEquals("inconsistent columns in matrix", exception.getMessage());

    }

    @Test
    public void givenOutOfBoundsIndexForRowOrColumnOrBoth_whenSubMatrix_thenThrowIllegalArgumentException() {
        int[][] input = new int[][]{
                {1, 7},
                {2, 5},
                {4, 7}
        };
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.subMatrix(input, 5, 5));
        Assertions.assertEquals("the given row and column are out of bounds", exception.getMessage());


        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.subMatrix(input, 5, 1));
        Assertions.assertEquals("the given row is out of bounds", exception.getMessage());

        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.subMatrix(input, 1, 5));
        Assertions.assertEquals("the given col is out of bounds", exception.getMessage());


    }

    @Test
    public void givenValidMatrixAndValidRowToRemoveAndValidColToRemove_whenSubMatrix_thenResultIsReturned() {
        int[][] input = new int[][]{
                {1, 7},
                {2, 5},
                {4, 7}
        };
        int rowToRemove = 1;
        int colToRemove = 0;

        Matrix expected = new Matrix(new int[][]{
                {7},
                {7}
        });


        Matrix actual = MatrixUtilities.subMatrix(input, rowToRemove, colToRemove);
        Assertions.assertEquals(expected, actual);

        int[][] input2 = new int[][]{
                {1, 7, 7, 8},
                {2, 7, 9, 5},
                {4, 2, 1, 7}
        };
        rowToRemove = 1;
        colToRemove = 2;

        expected = new Matrix(new int[][]{
                {1, 7, 8},
                {4, 2, 7}
        });
        actual = MatrixUtilities.subMatrix(input2, rowToRemove, colToRemove);
        Assertions.assertEquals(expected, actual);

        int[][] input3 = new int[][]{
                {1}
        };
        rowToRemove = 0;
        colToRemove = 0;

        expected = new Matrix(new int[][]{
        });
        actual = MatrixUtilities.subMatrix(input3, rowToRemove, colToRemove);
        Assertions.assertEquals(expected, actual);

    }

    @Test
    public void givenMatrixNotValid_whenSquareMatrix_thenIllegalArgumentExceptionIsThrown() {
        int[][] a = new int[][]{
                {1, 4, 5},
                null,
                {8, 2, 1}
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.squareMatrix(a, null));
        Assertions.assertEquals("row at index: 1 is null", exception.getMessage());

        int[][] b = new int[][]{
                {1, 4, 5},
                {1},
                {7, 2, 5}
        };

        exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.squareMatrix(b, null));
        Assertions.assertEquals("inconsistent columns in matrix", exception.getMessage());
    }

    @Test
    public void givenValidMatrixButNotSquareAndValidTriangular_whenSquareMatrix_thenThrowException() {
        int[][] input = new int[][]{
                {1, 4, 2, 4},
                {5, 2, 1, 4},
                {7, 1, 4, 11}
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.squareMatrix(input, null));
        Assertions.assertEquals("the given matrix is not square", exception.getMessage());

    }

    @Test
    public void givenValidSquareMatrixAndNullTriangular_whenSquareMatrix_thenThrowException() {
        int[][] input = new int[][]{
                {1, 4, 2, 4},
                {5, 2, 1, 4},
                {7, 1, 4, 11},
                {5, 11, 5, 5}
        };

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> MatrixUtilities.squareMatrix(input, null));
        Assertions.assertEquals("the given triangular is null", exception.getMessage());

    }

    @Test
    public void givenValidSquareMatrixAndValidTriangular_whenSquareMatrix_thenResultIsReturned() {
        //test diagonal
        int[][] input = new int[][]{
                {1, 4, 2, 4},
                {5, 2, 1, 4},
                {7, 1, 4, 11},
                {5, 11, 5, 5}
        };
        Matrix expected = new Matrix(new int[][]{
                {1, 0, 0, 0},
                {0, 2, 0, 0},
                {0, 0, 4, 0},
                {0, 0, 0, 5}
        });

        Matrix actual = MatrixUtilities.squareMatrix(input, Triangular.DIAGONAL);
        Assertions.assertEquals(expected, actual);

        //test LOWER

        expected = new Matrix(new int[][]{
                {1, 0, 0, 0},
                {5, 2, 0, 0},
                {7, 1, 4, 0},
                {5, 11, 5, 5}
        });

        actual = MatrixUtilities.squareMatrix(input, Triangular.LOWER);
        Assertions.assertEquals(expected, actual);

        //test UPPER

        expected = new Matrix(new int[][]{
                {1, 4, 2, 4},
                {0, 2, 1, 4},
                {0, 0, 4, 11},
                {0, 0, 0, 5}
        });

        actual = MatrixUtilities.squareMatrix(input, Triangular.UPPER);
        Assertions.assertEquals(expected, actual);


    }



}


